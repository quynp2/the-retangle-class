package com.devcamp.javabasic_j06.s10;

public class Retangle {
    public float length = 1.0f;
    public float width = 1.0f;

    public Retangle() {
    }

    public Retangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getArea() {
        return length * width;
    }

    public float getPerimeter() {
        return (length + width) * 2;
    }

    @Override
    public String toString() {
        return String.format("Retangle [ length = %s, width = %s]", length, width);
    }

}
