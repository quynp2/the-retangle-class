import com.devcamp.javabasic_j06.s10.Retangle;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Retangle hinhchunhat1 = new Retangle();

        Retangle hinhchunhat2 = new Retangle(3.0f, 4.0f);

        System.out.println(hinhchunhat1.toString());
        System.out.println(hinhchunhat2.toString());

        float dientich1 = hinhchunhat1.getArea();
        float dientich2 = hinhchunhat2.getArea();

        float chuvi1 = hinhchunhat1.getPerimeter();
        float chuvi2 = hinhchunhat2.getPerimeter();

        System.out.println("Dien tich hinh chu nhat = " + dientich1 + "chu vi hcn 1 = " + chuvi1);
        System.out.println("Dien tich hinh chu nhat = " + dientich2 + "chu vi hcn 2 = " + chuvi2);

    }
}
